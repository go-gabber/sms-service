# Gabber SMS
Gabber SMS - это микросервис который обеспечивает серверный SMS функционал габы



## ENVs
| Переменная     | Назначение         |
|----------------|--------------------|
| REST_PORT      | Порт для REST      | 
| REST_HOST      | Порт для тестов    |
| MONGO_USER     | Пользователь Mongo |
| MONGO_PASSWORD | Пароль Mongo	      |
| MONGO_DB       | БД Mongo           |
| MONGO_HOST     | Хост Mongo         |
| MONGO_PORT     | Порт Mongo         |

## REST
### Добавить пользователя
#### Request
```POST /add```
```
{
	"client":"asdf1234",		    // id-клиента
	"messages":[{
		"origin":"123456789",       // Номер отправителя
		"date":1245435,	            // Дата Unix-time
		"message":"клапана погнуло" // Душе-раздирающее сообщение
		}]
}	
```
#### Response
```200 OK```

### Получить сообщения 
```POST /get```
```
{
	"client", "asdf1234"	// id-клиента
}
```


#### Response
```200 OK```
```
"messages":[{
	"origin":"123456789",       // Номер отправителя
	"date":1245435,	            // Дата Unix-time
	"message":"клапана погнуло" // Душе-раздеряющее сообщение
}]
````

## Makefile
Так же для простоты использования можно запускать через Makefile
