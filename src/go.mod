module gabber_sms

go 1.16

require (
	github.com/golang/mock v1.5.0
	github.com/gorilla/mux v1.8.0
	go.mongodb.org/mongo-driver v1.5.3
	gotest.tools/v3 v3.0.3
)
