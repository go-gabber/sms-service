// db_interface - больше интерфейсов в жертву богу интерфейсов
package db_interface

//go:generate mockgen -source=db_interface.go -destination=../mocks/db.go

import (
	mongo "gabber_sms/db/mongo"
	models "gabber_sms/models"
)

// DBProvider provides db middleware for manager
type DBProvider interface {
	Connect() error
	Disconnect() error
	AddSMS(*models.SMSData) error
	GetSMS(string) (models.SMSData, error)
}

// GetDBHandler needed for select db driver
func GetDBHandler(handlerName string) DBProvider {
	switch handlerName {
	case "mongo-v1":
		return &mongo.Provider{}
	default:
		return &mongo.Provider{}
	}
}
