package gabber_mongo

import (
	"context"

	"log"
	"os"
	"time"

	models "gabber_sms/models"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoCreds struct {
	Host     string
	Port     string
	Password string
	User     string
	Db       string
}

type collections struct {
	clients *mongo.Collection
}

type Provider struct {
	connection *mongo.Client
	ctx        context.Context
	creds      MongoCreds
	collections
}

func (p *Provider) loadENV() {
	p.creds = MongoCreds{
		Host:     os.Getenv("MONGO_HOST"),
		Port:     os.Getenv("MONGO_PORT"),
		User:     os.Getenv("MONGO_USER"),
		Password: os.Getenv("MONGO_PASSWORD"),
		Db:       os.Getenv("MONGO_DB"),
	}
}

// +
func (p *Provider) Connect() error {
	p.loadENV()
	var cancel context.CancelFunc
	p.ctx, cancel = context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	var err error
	p.connection, err = mongo.Connect(p.ctx, options.Client().ApplyURI(
		"mongodb://"+p.creds.User+
			":"+p.creds.Password+
			"@"+p.creds.Host+
			":"+p.creds.Port,
	))

	if err != nil {
		log.Println("DB connecton error: ", err)
		return err
	}

	p.collections.clients = p.connection.Database(p.creds.Db).Collection("clients")

	log.Println("DB connection: success ! ")
	return nil
}

// -> ADD
func (p *Provider) Disconnect() error {
	// Для слабаков
	return nil
}

// +
func (p *Provider) AddSMS(sms *models.SMSData) error {
	for _, msg := range sms.Messages {
		p.push(p.collections.clients, msg, sms.Client)
	}
	return nil
}

// +
func (p *Provider) GetSMS(client string) (sms models.SMSData, err error) {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	getMessagesPipeline := mongo.Pipeline{
		bson.D{{Key: "$match", Value: bson.D{{Key: "client.id", Value: client}}}},
		bson.D{{Key: "$project", Value: bson.D{{Key: "messages", Value: "$client.messages"}}}},
	}

	cur, err := p.clients.Aggregate(ctx, getMessagesPipeline)
	if err != nil {
		panic(err)
	}
	defer cur.Close(ctx)
	var messages []models.SMSData
	if err = cur.All(ctx, &messages); err != nil {
		panic(err)
	}
	sms = messages[0] // Ой ну и ладно
	err = nil
	return
}

// +
func (p Provider) structToDoc(object interface{}) (document bson.D, err error) {
	log.Println("STD: ", object)
	data, err := bson.Marshal(object)
	if err != nil {
		log.Println("Mongo - structToDoc ERROR! : ", err)
		return nil, err
	}
	log.Println("MD: ", data)
	err = bson.Unmarshal(data, &document)
	if err != nil {
		log.Println("UMN ERROR ! :", err)
	}
	return
}

// +
func (p *Provider) push(collection *mongo.Collection, object interface{}, objectID string) error {
	newDocument, err := p.structToDoc(object)
	if err != nil {
		return err
	}

	log.Println(newDocument)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	filter := bson.M{"client.id": bson.M{"$eq": objectID}}
	update := bson.M{"$push": bson.M{"client.messages": newDocument}}

	res, err := collection.UpdateOne(ctx, filter, update)
	if err != nil {
		log.Println("Mongo - update error : ", err)
		return err
	}
	log.Println(res.UpsertedID)
	return nil
}
