package sms_api

import (
	"bytes"
	mock_db_interface "gabber_sms/mocks"
	"gabber_sms/models"
	"net/http/httptest"
	"testing"

	"github.com/golang/mock/gomock"
	"go.mongodb.org/mongo-driver/mongo"
	"gotest.tools/v3/assert"
)

func TestSMSAPI_AddSMS(t *testing.T) {
	type mockDB func(s *mock_db_interface.MockDBProvider, sms models.SMSData)
	cases := []struct {
		name           string
		requestBody    string
		requestStruct  models.SMSData
		mockDB         mockDB
		expectedStatus int
		expectedBody   string
	}{
		{
			name:        "Right",
			requestBody: `{"client":"testing","messages":[{"origin":"900","date":1245435,"message":"Четырка топ за свои деньги"}]}`,
			requestStruct: models.SMSData{
				Client: "testing",
				Messages: []models.SMS{
					{Origin: "900", Time: 1245435, Text: "Четырка топ за свои деньги"},
				},
			},
			mockDB: func(s *mock_db_interface.MockDBProvider, sms models.SMSData) {
				s.EXPECT().AddSMS(&sms).Return(nil)
			},
			expectedStatus: 200,
			expectedBody:   "",
		},
	}

	for _, testCase := range cases {
		t.Run(testCase.name, func(t *testing.T) {
			c := gomock.NewController(t)
			defer c.Finish()

			db := mock_db_interface.NewMockDBProvider(c)
			testCase.mockDB(db, testCase.requestStruct)
			api := SMSAPI{DB: db}

			w := httptest.NewRecorder()
			r := httptest.NewRequest("POST", "/add", bytes.NewBufferString(testCase.requestBody))

			api.AddSMS(w, r)

			assert.Equal(t, testCase.expectedStatus, w.Code)
			assert.Equal(t, testCase.expectedBody, w.Body.String())
		})
	}

}

func TestSMSAPI_GetSMS(t *testing.T) {
	type mockDB func(s *mock_db_interface.MockDBProvider, client string)
	cases := []struct {
		name           string
		requestBody    string
		requestStruct  models.GetRequest
		mockDB         mockDB
		expectedStatus int
		expectedBody   string
	}{
		{
			name:          "Right",
			requestBody:   `{"client":"testing"}`,
			requestStruct: models.GetRequest{Client: "testing"},
			mockDB: func(s *mock_db_interface.MockDBProvider, client string) {
				s.EXPECT().GetSMS(client).Return(models.SMSData{
					Client: "testing",
					Messages: []models.SMS{
						{Origin: "900", Time: 1245435, Text: "Четырка топ за свои деньги"},
					},
				}, nil)
			},
			expectedStatus: 200,
			expectedBody:   `{"client":"testing","messages":[{"origin":"900","date":1245435,"message":"Четырка топ за свои деньги"}]}`,
		},
		{
			name:          "No Content",
			requestBody:   `{"client":"kizyaky"}`,
			requestStruct: models.GetRequest{Client: "kizyaky"},
			mockDB: func(s *mock_db_interface.MockDBProvider, client string) {
				s.EXPECT().GetSMS(client).Return(models.SMSData{}, mongo.ErrNilDocument)
			},
			expectedStatus: 204,
			expectedBody:   "No Content bro\n",
		},
		{
			name:           "Decoder error",
			requestBody:    `{"clienyaky"}`,
			requestStruct:  models.GetRequest{Client: ""},
			mockDB:         func(s *mock_db_interface.MockDBProvider, client string) {},
			expectedStatus: 204,
			expectedBody:   "Decode error\n",
		},
	}

	for _, testCase := range cases {
		t.Run(testCase.name, func(t *testing.T) {
			c := gomock.NewController(t)
			defer c.Finish()

			db := mock_db_interface.NewMockDBProvider(c)
			testCase.mockDB(db, testCase.requestStruct.Client)
			api := SMSAPI{DB: db}

			w := httptest.NewRecorder()
			r := httptest.NewRequest("POST", "/get", bytes.NewBufferString(testCase.requestBody))

			api.GetSMS(w, r)

			assert.Equal(t, testCase.expectedStatus, w.Code)
			assert.Equal(t, testCase.expectedBody, w.Body.String())

		})
	}
}
