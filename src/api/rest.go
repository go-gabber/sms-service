package sms_api

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	db "gabber_sms/db"
	models "gabber_sms/models"

	"github.com/gorilla/mux"
)

type SMSAPI struct {
	DB db.DBProvider
}

type SMSAPIProvider interface {
	AddSMS(http.ResponseWriter, *http.Request)
	GetSMS(http.ResponseWriter, *http.Request)
}

func (s *SMSAPI) Run() error {
	s.DB.Connect()

	router := mux.NewRouter()

	router.HandleFunc("/add",
		s.AddSMS).Methods("POST")

	router.HandleFunc("/get",
		s.GetSMS).Methods("POST")

	http.Handle("/", router)
	err := http.ListenAndServe(":"+os.Getenv("REST_PORT"), nil)
	if err != nil {
		log.Fatal(err)
		return err
	}

	return nil
}

//+  AddSMS - Записывает сообщения из запроса в БД
func (s *SMSAPI) AddSMS(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var msg models.SMSData
	err := decoder.Decode(&msg)
	if err != nil {
		panic(err)
	}

	fmt.Println(&s.DB)
	s.DB.AddSMS(&msg)
	// Добавить ретурн 200 или 501
}

//+ GetSMS
func (s *SMSAPI) GetSMS(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var cli models.GetRequest
	err := decoder.Decode(&cli)
	if err != nil {
		http.Error(w, "Decode error", 204)
		return
	}

	var messages models.SMSData
	messages, err = s.DB.GetSMS(cli.Client)

	if err != nil {
		http.Error(w, "No Content bro", 204)
		return
	}
	returnData, _ := json.Marshal(messages)
	w.Write(returnData)
}
