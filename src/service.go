package main

import (
	rest "gabber_sms/api"
	db "gabber_sms/db/mongo"
)

func main() {
	restHandler := rest.SMSAPI{DB: &db.Provider{}}
	restHandler.Run()
}
