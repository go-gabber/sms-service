package models

// SMSData - Набор СМС от клиента
type SMSData struct {
	Client   string `json:"client"`
	Messages []SMS  `json:"messages" bson:"messages"`
}

// SMS - атомарное СМС
type SMS struct {
	Origin string `json:"origin" bson:"origin"`
	Time   uint64 `json:"date" bson:"date"`
	Text   string `json:"message" bson:"message"`
}

// GetRequest - Вспомнить бы еще
type GetRequest struct {
	Client string `json:"client"`
}
