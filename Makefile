include .env

# Очистить все
clean-all:
	rm -rf src/api/proto

# Запусить сервер
run: 
	cd src && go run ./

# Запусить БД
run-mongo-pod:
	podman pod start gabber-mongo 

# Тесты REST-API
run-tests: test-addsms test-getsms
	
test-addsms:
	curl  -X POST -d '{"client":"testing","messages":[{"origin":"900","date":1245435,"message":"Четырка топ за свои деньги"}]}' http://$$REST_HOST:$$REST_PORT/add

test-getsms:
	curl  -X POST -d '{"client":"testing"}' http://$$REST_HOST:$$REST_PORT/get

.PHONY: \
	run \
	clean-all
